/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.primitive;

import java.util.Scanner;

/**
 *
 * @author ShreyamDuttaGupta
 */
public class PrimitiveExamples {
    static public void main(String[] args){
        
        double num1,num2,ans=0;
        char opers;
        
        Scanner calc = new Scanner(System.in);
        System.out.print("Enter your First number");
        num1= calc.nextDouble();
        System.out.print("Enter your Second number");
        num2= calc.nextDouble();
        System.out.print("What operation(plus,minus,mult,div) do you what to perform?");
        opers= calc.next().charAt(0);
        
        if(opers == '+')
            ans=num1+num2;
        else if(opers == '-')
            ans=num1-num2;
        else if(opers == '*')
            ans=num1*num2;
        else if(opers == '/')
            ans=num1/num2;
        else
            System.out.print("Invalid operation");
        
        System.out.print("Answer = " + ans);
        
    }
}