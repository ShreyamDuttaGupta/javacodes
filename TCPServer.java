// TCPServer.java EECE6029 Cheng 2017
// an echo TCP server
// Usage: java TCPSever 12345

import java.io.*;
import java.util.*;
import java.util.concurrent.*;
import java.net.*;

public class TCPServer{
    static final int numberOfThreads = 3;
    static int MAXBF = 1024;
    
    static String delayedEcho(Socket sock){
        byte[] buffer = new byte[MAXBF];
        int recvSize = 0;
        try{
            SocketAddress clntAddr = sock.getRemoteSocketAddress();
            System.out.println(clntAddr + " connects");
            InputStream in = sock.getInputStream();
            recvSize = in.read(buffer);
            if (recvSize <= 0) return "";
            OutputStream out = sock.getOutputStream();
            out.write(buffer, 0, recvSize);
            sock.close();
            System.out.println(clntAddr + " completes");
        }catch (IOException e){ System.err.println(e.getMessage()); }
        return new String(buffer, 0, recvSize);
    }
    
    void server1(int port) throws IOException {  // serial server
        ServerSocket servSock = new ServerSocket(port);
        while (true){
            Socket clntSock = servSock.accept();
            delayedEcho(clntSock);
        }
    }
    
    void server2(int port) throws IOException { // one thread per client
        ServerSocket servSock = new ServerSocket(port);
        while (true){
            Socket clntSock = servSock.accept();
            Thread thread = new Thread(()->{ delayedEcho(clntSock); });
            thread.start();
        }
    }
    
    void server3(int port) throws IOException { // a pool of three serial threads
        ServerSocket servSock = new ServerSocket(port);
        for (int i = 0; i < numberOfThreads; i++){
            Thread thread = new Thread(()->{
                while (true){
                    try {
                        Socket clntSock = servSock.accept();
                        delayedEcho(clntSock);
                    } catch (IOException e){
                        System.err.println("IOException");
                        return;
                    }
                }});
            thread.start();
        }
    }
    
    void server4(int port) throws IOException { // cached or fixed thread pool via Executor
        ServerSocket servSock = new ServerSocket(port);
        //   ExecutorService service = Executors.newCachedThreadPool();
        //   ExecutorService service = Executors.newFixedThreadPool(numberOfThreads);
        ExecutorService service = Executors.newWorkStealingPool();
        while (true){
            Socket clntSock = servSock.accept();
            service.submit(()->{ delayedEcho(clntSock); });
        }
    }
    
    void server5(int port) throws IOException { // using Callable and Future
        ServerSocket servSock = new ServerSocket(port);
        ExecutorService service = Executors.newFixedThreadPool(numberOfThreads);
        HashSet<Future<String>> futures = new HashSet<Future<String>>();
        while (true){
            Socket clntSock = servSock.accept();
            futures.add(service.submit(()->{ return delayedEcho(clntSock); }));
            // check to see if a future in futures isDone
            // if so, get and print its return value and remove it from futures
        
            Iterator<Future<String>> iterator = futures.iterator();
            while(iterator.hasNext()){
                Future<String> future1 = iterator.next();
                if(future1.isDone()){
                    try {
                        System.out.println(future1.get());
                    } catch (InterruptedException | ExecutionException e) {
                        e.printStackTrace();
                    }
                    iterator.remove();
                }
            }
        }
    }
    
    
    public static void main(String[] args) throws IOException {
        if (args.length < 1){
            System.err.println("Usage: java TCPServer port");
            System.exit(1);
        }
        TCPServer s = new TCPServer();
        s.server5(Integer.parseInt(args[0]));
    }
}

