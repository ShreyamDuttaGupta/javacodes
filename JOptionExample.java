package com.mycompany.apples;
import javax.swing.JOptionPane;
public class JOptionExample {
    public static void main (String[] args){
        
        try{
            String s1= JOptionPane.showInputDialog("Enter the first number");
            String s2= JOptionPane.showInputDialog("Enter the second number");
            
            int num1= Integer.parseInt(s1);
            int num2= Integer.parseInt(s2);
            int sum=num1+num2;
            
            JOptionPane.showMessageDialog(null, "The sum is" +sum, "Addition", JOptionPane.PLAIN_MESSAGE);
        }catch(Exception e){
            e.printStackTrace();
            System.out.println("Wrong input");
        }
        
        
    }
}

